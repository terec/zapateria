object cortecaja: Tcortecaja
  Left = 155
  Top = 221
  BorderStyle = bsDialog
  Caption = 'Corte de Caja'
  ClientHeight = 292
  ClientWidth = 990
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 15
    Top = 13
    Width = 545
    Height = 117
  end
  object Bevel3: TBevel
    Left = 577
    Top = 130
    Width = 403
    Height = 111
  end
  object Bevel2: TBevel
    Left = 574
    Top = 13
    Width = 406
    Height = 111
  end
  object Label1: TLabel
    Left = 256
    Top = 115
    Width = 112
    Height = 13
    Caption = 'Total de Fondo de Caja'
  end
  object Label2: TLabel
    Left = 620
    Top = 107
    Width = 60
    Height = 13
    Caption = 'fecha Actual'
  end
  object Label5: TLabel
    Left = 586
    Top = 47
    Width = 132
    Height = 13
    Caption = 'Numero de Notas x Servicio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 870
    Top = 46
    Width = 85
    Height = 13
    Caption = 'Importe C/tarjeta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 58
    Top = 115
    Width = 103
    Height = 14
    Caption = 'Total de Efectivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 21
    Top = 23
    Width = 33
    Height = 13
    Caption = '$ 1000'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 131
    Top = 22
    Width = 27
    Height = 13
    Caption = '$ 500'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 239
    Top = 23
    Width = 27
    Height = 13
    Caption = '$ 200'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 348
    Top = 23
    Width = 27
    Height = 13
    Caption = '$ 100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 459
    Top = 23
    Width = 21
    Height = 13
    Caption = '$ 50'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label15: TLabel
    Left = 33
    Top = 52
    Width = 21
    Height = 13
    Caption = '$ 20'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label16: TLabel
    Left = 132
    Top = 51
    Width = 21
    Height = 13
    Caption = '$ 10'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel
    Left = 241
    Top = 51
    Width = 15
    Height = 13
    Caption = '$ 5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label19: TLabel
    Left = 465
    Top = 52
    Width = 15
    Height = 13
    Caption = '$ 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label24: TLabel
    Left = 734
    Top = 47
    Width = 111
    Height = 13
    Caption = 'Importe Total de Notas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label26: TLabel
    Left = 756
    Top = 106
    Width = 86
    Height = 13
    Caption = 'Imorte x  Efectivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label27: TLabel
    Left = 883
    Top = 106
    Width = 69
    Height = 13
    Caption = 'Total Otros FP'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label28: TLabel
    Left = 591
    Top = 170
    Width = 116
    Height = 13
    Caption = 'Numero de Notas x Vtas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label29: TLabel
    Left = 870
    Top = 159
    Width = 85
    Height = 13
    Caption = 'Importe C/tarjeta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label30: TLabel
    Left = 741
    Top = 159
    Width = 111
    Height = 13
    Caption = 'Importe Total de Notas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label31: TLabel
    Left = 756
    Top = 207
    Width = 86
    Height = 13
    Caption = 'Imorte x  Efectivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label32: TLabel
    Left = 883
    Top = 207
    Width = 69
    Height = 13
    Caption = 'Total Otros FP'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 353
    Top = 51
    Width = 15
    Height = 13
    Caption = '$ 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object dtp_notas: TDateTimePicker
    Left = 607
    Top = 80
    Width = 105
    Height = 21
    Date = 42284.592100787030000000
    Time = 42284.592100787030000000
    TabOrder = 0
  end
  object Button1: TButton
    Left = 607
    Top = 199
    Width = 73
    Height = 31
    Caption = 'Aplicar Cierre'
    TabOrder = 1
  end
  object totefec: TEdit
    Left = 47
    Top = 86
    Width = 128
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Text = '0'
  end
  object Edit1: TEdit
    Left = 251
    Top = 86
    Width = 124
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 3
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 139
    Width = 563
    Height = 124
    Color = clInfoBk
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'xventas'
        Title.Alignment = taCenter
        Title.Caption = 'Total X ventas'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 118
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'xservicio'
        Title.Alignment = taCenter
        Title.Caption = 'Total x servicio'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 129
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Fondo Fijo'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 125
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'totdia'
        Title.Alignment = taCenter
        Title.Caption = 'Total del dia'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 118
        Visible = True
      end>
  end
  object StatusBar2: TStatusBar
    Left = 0
    Top = 273
    Width = 990
    Height = 19
    Panels = <>
    ExplicitTop = 365
    ExplicitWidth = 985
  end
  object ed100: TEdit
    Left = 381
    Top = 18
    Width = 66
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 6
  end
  object ed500: TEdit
    Left = 167
    Top = 18
    Width = 66
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 7
  end
  object ed1000: TEdit
    Left = 60
    Top = 18
    Width = 68
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 8
  end
  object ed200: TEdit
    Left = 273
    Top = 18
    Width = 66
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 9
  end
  object ed50: TEdit
    Left = 487
    Top = 18
    Width = 66
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 10
  end
  object ed20: TEdit
    Left = 60
    Top = 47
    Width = 66
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 11
  end
  object ed10: TEdit
    Left = 167
    Top = 47
    Width = 68
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 12
  end
  object ed5: TEdit
    Left = 273
    Top = 47
    Width = 66
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 13
  end
  object ed1: TEdit
    Left = 381
    Top = 47
    Width = 67
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 14
  end
  object totnotas: TEdit
    Left = 733
    Top = 18
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
    Text = '0'
  end
  object nnotastsf: TEdit
    Left = 620
    Top = 18
    Width = 53
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
    Text = '0'
  end
  object tottnotas: TEdit
    Left = 858
    Top = 18
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 17
    Text = '0'
  end
  object efecnotas: TEdit
    Left = 740
    Top = 78
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 18
    Text = '0'
  end
  object tototrosp: TEdit
    Left = 858
    Top = 78
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 19
    Text = '0'
  end
  object Edit2: TEdit
    Left = 740
    Top = 130
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 20
    Text = '0'
  end
  object Edit6: TEdit
    Left = 621
    Top = 141
    Width = 53
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 21
    Text = '0'
  end
  object Edit7: TEdit
    Left = 858
    Top = 130
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 22
    Text = '0'
  end
  object Edit8: TEdit
    Left = 742
    Top = 178
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 23
    Text = '0'
  end
  object Edit9: TEdit
    Left = 860
    Top = 178
    Width = 112
    Height = 23
    Alignment = taCenter
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 24
    Text = '0'
  end
  object Edit3: TEdit
    Left = 486
    Top = 47
    Width = 67
    Height = 23
    Alignment = taCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    MaxLength = 3
    NumbersOnly = True
    ParentFont = False
    TabOrder = 25
  end
end
