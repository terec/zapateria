object consul_clientes: Tconsul_clientes
  Left = 335
  Top = 233
  Caption = 'consul_clientes'
  ClientHeight = 277
  ClientWidth = 582
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 13
    Width = 37
    Height = 13
    Caption = 'Cliente:'
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 42
    Width = 582
    Height = 235
    Align = alBottom
    DataSource = Ds_consutas_cliente
    Options = [dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgMultiSelect, dgTitleHotTrack]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Alignment = taCenter
        Color = clGradientInactiveCaption
        Expanded = False
        FieldName = 'idc'
        Title.Alignment = taCenter
        Title.Caption = 'Clave'
        Title.Color = clMoneyGreen
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Times New Roman'
        Title.Font.Style = [fsItalic]
        Width = 102
        Visible = True
      end
      item
        Alignment = taCenter
        Color = clGradientInactiveCaption
        Expanded = False
        FieldName = 'nombre'
        Title.Alignment = taCenter
        Title.Color = clMoneyGreen
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = 'Times New Roman'
        Title.Font.Style = [fsItalic]
        Width = 464
        Visible = True
      end>
  end
  object Edit1: TEdit
    Left = 87
    Top = 8
    Width = 343
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
  end
  object Ds_consutas_cliente: TDataSource
    DataSet = dm1.cds_clientes
    Left = 56
    Top = 88
  end
end
