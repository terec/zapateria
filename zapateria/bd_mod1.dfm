object dm1: Tdm1
  OldCreateOrder = False
  Left = 683
  Top = 456
  Height = 227
  Width = 339
  object cds_clientes: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 64
    Top = 16
    Data = {
      AA0000009619E0BD010000001800000006000000000003000000AA0003696463
      0400010000000000066E6F6D6272650100490000000100055749445448020002
      00640009646972656363696F6E010049000000010005574944544802000200C8
      0002637001004900000001000557494454480200020006000774656C66696A6F
      01004900000001000557494454480200020014000363656C0100490000000100
      0557494454480200020010000000}
    object cds_clientesidc: TIntegerField
      FieldName = 'idc'
    end
    object cds_clientesnombre: TStringField
      FieldName = 'nombre'
      Size = 100
    end
    object cds_clientesdireccion: TStringField
      FieldName = 'direccion'
      Size = 200
    end
    object cds_clientescp: TStringField
      FieldName = 'cp'
      Size = 6
    end
    object cds_clientestelfijo: TStringField
      FieldName = 'telfijo'
    end
    object cds_clientescel: TStringField
      FieldName = 'cel'
      Size = 16
    end
  end
  object ClientDataSet2: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64
    Top = 96
  end
  object DataSetProvider2: TDataSetProvider
    Left = 184
    Top = 104
  end
end
