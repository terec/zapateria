object tipos_servicios: Ttipos_servicios
  Left = 356
  Top = 240
  BorderStyle = bsDialog
  Caption = 'Tipos de Servicio'
  ClientHeight = 324
  ClientWidth = 401
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 13
    Top = 10
    Width = 381
    Height = 49
    Style = bsRaised
  end
  object Label8: TLabel
    Left = 37
    Top = 43
    Width = 11
    Height = 13
    Caption = 'ID'
  end
  object Label9: TLabel
    Left = 154
    Top = 43
    Width = 54
    Height = 13
    Caption = 'Descripcion'
  end
  object SpeedButton1: TSpeedButton
    Left = 361
    Top = 16
    Width = 27
    Height = 23
    Caption = '+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = SpeedButton1Click
  end
  object Label1: TLabel
    Left = 304
    Top = 43
    Width = 29
    Height = 13
    Caption = 'Precio'
  end
  object Descrip: TEdit
    Left = 66
    Top = 16
    Width = 226
    Height = 19
    TabOrder = 0
  end
  object DBGrid2: TDBGrid
    Left = 12
    Top = 74
    Width = 382
    Height = 232
    DataSource = ds_cds_tipos
    DrawingStyle = gdsClassic
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Color = cl3DLight
        Expanded = False
        FieldName = 'id'
        Title.Alignment = taCenter
        Title.Caption = 'ID'
        Width = 43
        Visible = True
      end
      item
        Color = cl3DLight
        Expanded = False
        FieldName = 'descripcion'
        Title.Alignment = taCenter
        Title.Caption = 'Descripcion'
        Width = 251
        Visible = True
      end
      item
        Color = cl3DLight
        Expanded = False
        FieldName = 'precio'
        Title.Alignment = taCenter
        Title.Caption = 'Precio'
        Visible = True
      end>
  end
  object prec1: TEdit
    Left = 298
    Top = 16
    Width = 43
    Height = 19
    TabOrder = 1
  end
  object DBEdit1: TDBEdit
    Left = 18
    Top = 16
    Width = 42
    Height = 19
    Color = clInfoBk
    DataField = 'id'
    DataSource = ds_cds_tipos
    Enabled = False
    TabOrder = 3
  end
  object cds_tipo_serv: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 72
    Top = 104
    Data = {
      680000009619E0BD010000001800000003000000000003000000680002696404
      000100000000000B6465736372697063696F6E01004900000001000557494454
      48020002005A000670726563696F080004000000010007535542545950450200
      490006004D6F6E6579000000}
    object cds_tipo_servid: TAutoIncField
      FieldName = 'id'
    end
    object cds_tipo_servdescripcion: TStringField
      FieldName = 'descripcion'
      Size = 90
    end
    object cds_tipo_servprecio: TCurrencyField
      FieldName = 'precio'
    end
  end
  object ds_cds_tipos: TDataSource
    DataSet = cds_tipo_serv
    Left = 160
    Top = 120
  end
end
