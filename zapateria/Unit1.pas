unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, Grids, DBGrids, ToolWin,
  ActnMan, ActnCtrls, ActnList, XPStyleActnCtrls, ImgList, Mask, DBCtrls;

type
  TForm1 = class(TForm)
    DBGrid3: TDBGrid;
    DTP_DE: TDateTimePicker;
    DBGrid6: TDBGrid;
    DTP_HASTA: TDateTimePicker;
    Label4: TLabel;
    Button1: TButton;
    ImageList1: TImageList;
    Menugeneral: TActionManager;
    Report: TAction;
    servicio: TAction;
    salida: TAction;
    Inventario1: TAction;
    Clientes: TAction;
    ControlAction1: TControlAction;
    Cortecaja: TAction;
    Mantenimiento: TAction;
    ActionToolBar1: TActionToolBar;
    enombre: TEdit;
    Label1: TLabel;
    Label5: TLabel;
    StatusBar1: TStatusBar;
    procedure servicioExecute(Sender: TObject);
    procedure Inventario1Execute(Sender: TObject);
    procedure ClientesExecute(Sender: TObject);
    procedure CortecajaExecute(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
            uses
              zservicio,inventario,clientes,cortecaj,
{$R *.dfm}    consulta_clientes;

procedure TForm1.ClientesExecute(Sender: TObject);
begin
  zclientes.ShowModal;
end;

procedure TForm1.CortecajaExecute(Sender: TObject);
begin
   cortecaj.cortecaja.ShowModal;
end;

procedure TForm1.DBGrid3DblClick(Sender: TObject);
begin
  forma_colsultacli.ShowModal;
end;

procedure TForm1.Inventario1Execute(Sender: TObject);
begin
  zinventario.ShowModal;
end;

procedure TForm1.servicioExecute(Sender: TObject);
begin
    servicio1.ShowModal;
end;

end.
