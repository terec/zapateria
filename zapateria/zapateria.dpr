program zapateria;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  clientes in 'clientes.pas' {zclientes},
  zservicio in 'zservicio.pas' {servicio1},
  inventario in 'inventario.pas' {zinventario},
  consul1_serv in 'consul1_serv.pas' {consul_clientes},
  rpago in 'rpago.pas' {cierrenota},
  cortecaj in 'cortecaj.pas' {cortecaja},
  bd_mod1 in 'bd_mod1.pas' {dm1: TDataModule},
  tipos_ser in 'tipos_ser.pas' {tipos_servicios},
  produc1 in 'produc1.pas' {productos},
  consulta_clientes in 'consulta_clientes.pas' {forma_colsultacli},
  c_servicios in 'c_servicios.pas' {tipospr};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(Tzclientes, zclientes);
  Application.CreateForm(Tservicio1, servicio1);
  Application.CreateForm(Tzinventario, zinventario);
  Application.CreateForm(Tconsul_clientes, consul_clientes);
  Application.CreateForm(Tcierrenota, cierrenota);
  Application.CreateForm(Tcortecaja, cortecaja);
  Application.CreateForm(Tdm1, dm1);
  Application.CreateForm(Ttipos_servicios, tipos_servicios);
  Application.CreateForm(Tproductos, productos);
  Application.CreateForm(Tforma_colsultacli, forma_colsultacli);
  Application.CreateForm(Ttipospr, tipospr);
  Application.Run;
end.
