unit clientes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ImgList, ActnList, PlatformDefaultStyleActnCtrls, ActnMan, Grids,
  DBGrids, ComCtrls, StdCtrls, ExtCtrls, ToolWin, ActnCtrls;

type
  Tzclientes = class(TForm)
    ActionToolBar1: TActionToolBar;
    direccion: TLabeledEdit;
    telfijo: TLabeledEdit;
    cp: TLabeledEdit;
    StatusBar1: TStatusBar;
    cel: TLabeledEdit;
    EDNOM: TLabeledEdit;
    DBGrid1: TDBGrid;
    ActionManager1: TActionManager;
    Action2: TAction;
    iconos: TImageList;
    ds_cds_clientes: TDataSource;
    idencliente: TLabel;
    procedure limpiar_casillas;
    procedure llenar_casilla;
    procedure Action2Execute(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  zclientes: Tzclientes;

implementation
            uses bd_mod1;
{$R *.dfm}

procedure Tzclientes.llenar_casilla;

  begin
  limpiar_casillas;
  with dm1.cds_clientes do
    begin
       idencliente.Caption:=trim(fieldbyname('idc').Asstring);
       ednom.Text:=fieldbyname('nombre').Asstring;
       direccion.Text:=fieldbyname('direccion').Asstring;
       cp.Text:=fieldbyname('cp').Asstring;
       telfijo.Text:=fieldbyname('telfijo').Asstring;
       cel.Text:=fieldbyname('cel').Asstring;
    end;
  end;

procedure Tzclientes.DBGrid1DblClick(Sender: TObject);
 begin
  if dm1.cds_clientes.RecordCount <> 0 then
    llenar_casilla
  else
    showmessage('No hay registros para mostrar');
end;

procedure Tzclientes.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_f5  then
    begin
     if messagedlg('Eliminar el Registro ?',mterror,[mbYes,mbNO], 0) = mrYes then
       dm1.cds_clientes.Delete
    end;
end;

procedure Tzclientes.limpiar_casillas;
  begin
    ednom.clear; direccion.clear;
    cp.clear;telfijo.clear; cel.clear;
    ednom.setfocus;  idencliente.Caption:='0'; // guarda el id
  end;

procedure Tzclientes.Action2Execute(Sender: TObject);
 var
   idxc,pos:integer;
begin
  if (trim(ednom.text) = '') or  (trim(direccion.text) = '') or
     (trim(cel.text) = '')then
    begin
      showmessage ('El Nombre, la Direccion y el Celular son campos Obligatorios,' +
                   ' asignarles valores para Guardar el Registro')
    end
  else
   begin
    if messagedlg('Guardar Registro ?',mterror,[mbYes,mbNO], 0) = mrYes then
      begin
       with dm1.cds_clientes do
        begin
        idxc:=1;
        pos:=recno; // posicion del registro
        if recordcount > 0  then
          begin

            last;
            idxc:=fieldbyname('idc').AsInteger + 1;
          end;
          if idencliente.Caption = '0' then
           begin
             append;
             edit;
             fieldbyname('idc').AsInteger:=idxc
           end
          else
           begin
            recno:=pos;
            edit;
           end;
         fieldbyname('nombre').Asstring:=trim(ednom.Text);
         fieldbyname('direccion').Asstring:=trim(direccion.Text);
         fieldbyname('cp').Asstring:=trim(cp.Text);
         fieldbyname('telfijo').Asstring:=trim(telfijo.Text);
         fieldbyname('cel').Asstring:=trim(cel.Text);
         next;

         showmessage('Registro Guardado');
         limpiar_casillas;
        end;
      end;
   end;
  end;
end.
