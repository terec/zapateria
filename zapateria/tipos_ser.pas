unit tipos_ser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, Grids, DBGrids, StdCtrls, DB, DBClient, ExtCtrls, Mask,
  DBCtrls;

type
  Ttipos_servicios = class(TForm)
    Label8: TLabel;
    Label9: TLabel;
    Descrip: TEdit;
    DBGrid2: TDBGrid;
    SpeedButton1: TSpeedButton;
    cds_tipo_serv: TClientDataSet;
    ds_cds_tipos: TDataSource;
    prec1: TEdit;
    Label1: TLabel;
    cds_tipo_servdescripcion: TStringField;
    cds_tipo_servprecio: TCurrencyField;
    cds_tipo_servid: TAutoIncField;
    Bevel1: TBevel;
    DBEdit1: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  tipos_servicios: Ttipos_servicios;

implementation

{$R *.dfm}

procedure Ttipos_servicios.SpeedButton1Click(Sender: TObject);
begin
  with  cds_tipo_serv do
   begin
      append;
      edit;
      fieldbyname('descripcion').AsString:= trim(descrip.Text);
      fieldbyname('precio').Ascurrency:=strtocurr(prec1.Text);
      next;
   end;
end;

end.
